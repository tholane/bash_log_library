#!/usr/bin/env bash 
#===============================================================================
#
#          FILE: bash_log_lib.sh
# 
#         USAGE: This file is itended to be included in scripts that require logging
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: nucurses (for the tput command)
#                
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/11/2016 10:09
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function bash_log_init {

  #Declare priorities for use by logging programs
  declare -grx EMERGENCY="EMERGENCY"
  declare -grx ALERT="ALERT"
  declare -grx CRITICAL="CRITICAL"
  declare -grx ERROR="ERROR"
  declare -grx WARNING="WARNING"
  declare -grx NOTICE="NOTICE"
  declare -grx INFO="INFO"
  declare -grx DEBUG="DEBUG"
  declare -grx NONE="NONE"

  #Declare the log destinations
  declare -grx JOURNAL="JOURNAL"
  declare -grx SYSLOG="SYSLOG"
  declare -grx STD_ERR="STD_ERR"
  declare -grx STD_OUT="STD_OUT"

  #Set the defualt LOG_LEVEL to DEBUG 
  declare -gx LOG_LEVEL=$DEBUG

  #Set the defualt LOG_DEST to standard error
  declare -gx LOG_DEST=$STD_ERR


  #Export the log function so it can be called by child processes
  export -f log

}

#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  log
#   DESCRIPTION: outputs log to specified backend: 
#                 systemd journal, syslog, standard error, standard output,a file  
#               Log entries include the message and the calling script path 
#    PARAMETERS: p) Log priority as a name see list above
#                2) message to be logged
#       RETURNS:  
#-------------------------------------------------------------------------------
function log() {

 #Arrays can't be exported so the following need to be declared every time the 
  #log function is invoked by a child process
  declare -ar LOG_LEVELS=(EMERGENCY ALERT CRITICAL ERROR WARNING NOTICE INFO DEBUG NONE)
  declare -Ar LOG_LEVEL_NAMES=([EMERGENCY]=0 [ALERT]=1 [CRITICAL]=2 [ERROR]=3 [WARNING]=4 [NOTICE]=5 [INFO]=6 [DEBUG]=7 [NONE]=8 )

  #set default priority
  declare priority=${LOG_LEVEL_NAMES[$INFO]}
 
  declare -i OPTIND=1 #reset option index so that secondary calls to log will process all options
  while getopts p: option; do
    case $option in
      p) priority=${LOG_LEVEL_NAMES[$OPTARG]};; #Overide priority with parameter 
      *) 
          printf "Invalid log parameter" >&2
          return 1
        ;;

    esac    # --- end of case ---
  done

  shift $(($OPTIND-1)) #move to the first non-option argument
  declare message=$1 
  declare msg_prefix
  declare msg_suffix
  declare frmt_string
  declare tput_color

  #If no terminal is set don't worry about configuring terminal logging options
  if [ -z "$TERM" ]; then


    #---  FUNCTION  ----------------------------------------------------------------
    #          NAME:  entry_setup
    #   DESCRIPTION:  Configures entry entry prefix and suffix
    #         NOTES:  - a nested function is used to allow access to general log variables
    #                   without having to export them.
    #                 - makes use of caller bash builtin to record line number and
    #                   filename of the script generating the log
    #    PARAMETERS: - 
    #       RETURNS: - 
    #-------------------------------------------------------------------------------
    function entry_setup(){
      declare -a tput_set
      case ${LOG_LEVELS[$priority]} in
        EMERGENCY)
          frmt_string="\n%s\n\n"
          msg_prefix="*** EMERGENCY:"
          msg_suffix="(line $(caller 1)) ***"
          ;;

        ALERT)
          frmt_string="\n%s\n\n"
          msg_prefix="*** ALERT:"
          msg_suffix="(line $(caller 1)) ***"
          ;;

        CRITICAL)
          frmt_string="\n%s\n\n"
          msg_prefix="*** CRITICAL:"
          msg_suffix="(line $(caller 1)) ***"
          ;;

        ERROR)
          frmt_string="\n%s\n\n"
          msg_prefix="*** ERROR:"
          msg_suffix="(line $(caller 1)) ***"
          ;;

        WARNING)
          frmt_string="\n%s\n"
          msg_prefix="=== WARNING:"
          msg_suffix="(line $(caller 1)) ==="
          ;;

        NOTICE)
          frmt_string="\n%s\n"
          msg_prefix="___ NOTICE:"
          msg_suffix="(line $(caller 1)) ___"
          ;;

        INFO)
          frmt_string="\n%s\n"
          msg_prefix="___ INFO:"
          msg_suffix="(line $(caller 1)) ___"
          ;;

        DEBUG)
          frmt_string="%s\n"
          msg_prefix="DEBUG:"
          msg_suffix="(line $(caller 1))"
          ;;

        *)
          frmt_string="\t%s\n"
          msg_prefix="NO LEVEL:"
          msg_suffix="(line $(caller 1))"
          ;;

      esac    # --- end of case ---
     
    }
    

    #---  FUNCTION  ----------------------------------------------------------------
    #          NAME:  entry_cleanup
    #   DESCRIPTION:  resets terminal properties to default
    #    PARAMETERS: - 
    #       RETURNS: - 
    #-------------------------------------------------------------------------------
    function entry_cleanup(){
      return 0
    }


 
  else

    #use the following command to get a full list of colors in your terminal
    #( x=`tput op` y=`printf %$((${COLUMNS}-6))s`;for i in {0..256};do o=00$i;echo -e ${o:${#o}-3:3} `tput setaf $i;tput setab $i`${y// /=}$x;done; )
    if [[ $(tput colors) -eq 256 ]]; then
      declare -Ar COLORS=([red]=196 [green]=002 [yellow]=184 [blue]=004 [magenta]=005 [cyan]=006 [white]=007 [default]=256)
    else
      declare -Ar COLORS=([red]=1 [green]=2 [yellow]=3 [blue]=4 [magenta]=5 [cyan]=6 [white]=7 [default]=9)
    fi


    #---  FUNCTION  ----------------------------------------------------------------
    #          NAME:  entry_setup
    #   DESCRIPTION:  Configures entry terminal properties based on log entry priority 
    #         NOTES:  - a nested function is used to allow access to general log variables
    #                   without having to export them.
    #                 - makes use of caller bash builtin to record line number and
    #                   filename of the script generating the log
    #    PARAMETERS: - 
    #       RETURNS: - 
    #-------------------------------------------------------------------------------
    function entry_setup(){
      declare -a tput_set
      case ${LOG_LEVELS[$priority]} in
        EMERGENCY)
          tput_set=(bold smul)
          frmt_string="\n%s\n\n"
          tput_color=red
          msg_prefix="*** EMERGENCY:"
          msg_suffix="(line $(caller 1)) ***"
          ;;

        ALERT)
          tput_set=(bold)
          frmt_string="\n%s\n\n"
          tput_color=red
          msg_prefix="*** ALERT:"
          msg_suffix="(line $(caller 1)) ***"
          ;;

        CRITICAL)
          tput_set=(smul)
          frmt_string="\n%s\n\n"
          tput_color=red
          msg_prefix="*** CRITICAL:"
          msg_suffix="(line $(caller 1)) ***"
          ;;

        ERROR)
          tput_set=(sgr0)
          frmt_string="\n%s\n\n"
          tput_color=red
          msg_prefix="*** ERROR:"
          msg_suffix="(line $(caller 1)) ***"
          ;;

        WARNING)
          tput_set=(bold)
          frmt_string="\n%s\n"
          tput_color=yellow
          msg_prefix="=== WARNING:"
          msg_suffix="(line $(caller 1)) ==="
          ;;

        NOTICE)
          tput_set=(sgr0)
          tput_color=yellow
          frmt_string="\n%s\n"
          msg_prefix="___ NOTICE:"
          msg_suffix="(line $(caller 1)) ___"
          ;;

        INFO)
          tput_set=(smul)
          tput_color=green
          frmt_string="\n%s\n"
          msg_prefix="___ INFO:"
          msg_suffix="(line $(caller 1)) ___"
          ;;

        DEBUG)
          tput_set=(sgr0)
          frmt_string="%s\n"
          tput_color=green
          msg_prefix="DEBUG:"
          msg_suffix="(line $(caller 1))"
          ;;

        *)
          tput_set=(sgr0)
          frmt_string="\t%s\n"
          tput_color=default
          msg_prefix="NO LEVEL:"
          msg_suffix="(line $(caller 1))"
          ;;

      esac    # --- end of case ---
      for setting in ${tput_set[@]}; do
        tput $setting
      done
      # color must come after options - default resets everyting
      tput setaf ${COLORS[$tput_color]}
      
    }
  
    #---  FUNCTION  ----------------------------------------------------------------
    #          NAME:  entry_cleanup
    #   DESCRIPTION:  resets terminal properties to default
    #    PARAMETERS: - 
    #       RETURNS: - 
    #-------------------------------------------------------------------------------
    function entry_cleanup(){
      #set terminal to default
      tput sgr0
    }

  fi


  #---  FUNCTION  ----------------------------------------------------------------
  #          NAME:  record_entry
  #   DESCRIPTION: Records log entry to configured back end  
  #         NOTES: a specific record_entry function is delcared based on the 
  #                logging destination
  #    PARAMETERS: - 
  #       RETURNS: -
  #-------------------------------------------------------------------------------
  case $LOG_DEST in
    JOURNAL)
      function record_entry {
        if (( $priority <= ${LOG_LEVEL_NAMES[$LOG_LEVEL]} )); then
          #systemd-cat priorities need to be numeric 
          systemd-cat -p $((priority)) -t log_lib printf "%s" "$message $msg_suffix" 
        fi
      }
      ;;

    SYSLOG)
      function record_entry {
        if (( $priority <= ${LOG_LEVEL_NAMES[$LOG_LEVEL]} )) ; then
          logger -i -p $((priority)) -t log_lib  "$message $msg_suffix" 
        fi
      }
      ;;

    STD_OUT)
      function record_entry {
        if (( $priority <= ${LOG_LEVEL_NAMES[$LOG_LEVEL]} )) ; then
          printf $frmt_string "$msg_prefix $message $msg_suffix" >&1
        fi
      }
      ;;

    STD_ERR)
      function record_entry {
        if (( $priority <= ${LOG_LEVEL_NAMES[$LOG_LEVEL]} )) ; then
          printf $frmt_string "$msg_prefix $message $msg_suffix" >&2
        fi
      }
      ;;

    *) #otherwise output to the specified file
      function record_entry(){
        #write  to defualt location if destination specified but empty
        if [[ -z "${LOG_DEST}" ]] ; then
          declare -x LOG_DEST=log_lib_out.txt
        fi

        if (( $priority <= ${LOG_LEVEL_NAMES[$LOG_LEVEL]} )) ; then
          printf $frmt_string "$msg_prefix $message $msg_suffix" >> $LOG_DEST
        fi
      }
      ;;
  esac    # --- end of case logging destination ---

  #Actually do the logging
  entry_setup $priority
  record_entry 
  entry_cleanup

}

bash_log_init
