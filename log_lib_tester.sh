#!/usr/bin/env bash 
#===============================================================================
#
#          FILE: log_lib_tester.sh
# 
#         USAGE: ./log_lib_tester.sh 
# 
#   DESCRIPTION: tests the log_lib.sh library
# 
#       OPTIONS: -a : record log entries at all log levels 
#                -b : record log entries using all destinations
#                -l log_level : the minimum severity to send to the log destination
#                -d log_destination: the backend to output log entries to.
#
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/11/2016 11:20
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error

#Source the library
source $(dirname $0)/bash_log_lib.sh

function generate_entries { 
#Generate each of the log types
  log "Default Priority Message"
  log -p $EMERGENCY "Emergency Message"
  log -p $ALERT "Alert Message"
  log -p $CRITICAL "Critical Message"
  log -p $ERROR "Error Message"
  log -p $WARNING "Warning Message"
  log -p $NOTICE "Notice Message"
  log -p $INFO "Info Message"
  log -p $DEBUG "Debug Message"
}

function main {
  # Process log level and destination
  OPTIND=1 # reset option index, necessary on repeated calls to function
  while getopts abl:d: option; do
    case $option in
      a)
        log_level_all="TRUE"
        break
        ;;
      b)
        log_dest_all="TRUE"
        break
        ;;
      l)
        log_level=$OPTARG;;
      d)
        log_dest=$OPTARG;;
      *)
        log $ERROR "Invalid arg"
        exit 1
        ;;
    esac
  done


  if [[ ${log_level_all:+1} ]] ; then #log entries at all log levels

    echo "Testing all log levels"
    declare -a log_levels=(EMERGENCY ALERT CRITICAL ERROR WARNING NOTICE INFO DEBUG NONE)
    for level in ${log_levels[@]} ; do
      echo "Testing log level: $level"
      declare -x LOG_LEVEL=$level
      generate_entries
    done

  elif [[ ${log_dest_all:+1} ]] ; then #log entries using all destinations

    echo "Testing all destinations"
    declare -a log_destinations=(JOURNAL SYSLOG STD_ERR STD_OUT)
    for dest in ${log_destinations[@]} ; do
      echo "Testing log destination: $dest"
      declare -x LOG_DEST=$dest
      generate_entries
    done

  else                                #log entries using defaults or parameters

    #Set logging level or assign the default of debug
    if [[ ${log_level:+1} ]]; then
      declare -x LOG_LEVEL=$log_level
    else
      declare -x LOG_LEVEL=$DEBUG
#Export the log function so it can be called by child processes
export -f log
    fi

    #Set logging destination or assign the default of Standard Error
    if [[ ${log_dest:+1} ]]; then
      declare -x LOG_DEST=$log_dest
    else
      declare -x LOG_DEST=$STD_ERR
    fi

    generate_entries
  fi
}

main "$@"
